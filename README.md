# Assignment 2 - Agile Software Practice.
​
Name: Mark Campbell
​
## API endpoints.
 
### Movies 
+ /api/movies/tmdb/movie/:id | GET | Gets a single movie from tmdb
+ /api/movies/tmdb/discover | GET | Get discover movies from tmdb
+ /api/movies/tmdb/upcoming | GET | Get upcoming movies from tmdb
+ /api/movies/tmdb/now-playing | GET | Get now playing movies from tmdb
+ /api/movies/tmdb/top-rated | GET | Get top rated movies from tmdb
+ /api/movies/tmdb/movie/:id/recommended | GET | Get recommended movies from tmdb
+ /api/movies/tmdb/movie/:id/similar | GET | Get similar movies from tmdb
+ /api/movies/tmdb/movie/:id/credits | GET | Get movies credits from tmdb
+ /api/movies/tmdb/trending | GET | Get trending movies credits from tmdb
+ /api/movies/tmdb/movie/:id/images | GET | Get movie images from tmdb

### Actors
+ /api/actors/tmdb/popular | GET | Gets popular actors from tmdb
+ /api/actors/tmdb/trending | GET | Gets trending actors from tmdb
+ /api/actors/tmdb/actor/:id | GET | Gets a single actor from tmdb
+ /api/actors/tmdb/actor/:id/movie-credits | GET | Gets movie credits for an actor from tmdb
+ /api/actors/tmdb/actor/:id/images | GET | Gets actors images from tmdb

### Users
+ /api/users | GET | Gets a single movie from tmdb
+ /api/users/ | GET | Gets users 
+ /api/users/ | POST | Registers/authenticates a user
+ /api/users/:userName/favourites | POST | Add a favourite movieId to user's favourites array
+ /api/users/:username/movie/:id/favourites | POST | Deletes a movieId from a user's favourites array
- /api/users/:userName/favourites | GET | Gets users favourites 

### Genres
+ /api/genres/ | GET | Gets genres from tmdb

### Reviews
+ /api/reviews/movie/:id/reviews | GET | Gets movie reviews
+ /api/reviews/:username/movie/:id/reviews | POST | creates/updates a movie review

​
## Test cases.

~~~
  Movies endpoint
    GET /api/movies/tmdb/discover 
      ✓ should return 20 movies and a status 200 (826ms)
    GET /api/movies/tmdb/upcoming 
      ✓ should return 20 movies and a status 200 (115ms)
    GET /api/movies/tmdb/now-playing 
      ✓ should return 20 movies and a status 200 (119ms)
    GET /api/movies/tmdb/top-rated 
      ✓ should return 20 movies and a status 200 (108ms)
    GET /api/movies/tmdb/trending 
      ✓ should return 20 movies and a status 200 (131ms)
    GET /api/movies/tmdb/movie/:id
      ✓ should return the matching movie (120ms)
    GET /api/movies/tmdb/movie/:id/similar
      ✓ should return similar movies (185ms)
    GET /api/movies/tmdb/movie/:id/recommended
      ✓ should return recommended movies (121ms)
    GET /api/movies/tmdb/movie/:id/credits
      ✓ should return the list of actors assosiated with a movie (152ms)

  Reviews endpoint
    GET /api/reviews/movie/:id/reviews 
      ✓ should return 2 reviews for Black Adam and a status 200
    POST /api/reviews/:username/movie/:id/reviews 
      ✓ should update the review for Black Adam made by user1 and return status 200 (111ms)
    POST /api/reviews/:username/movie/:id/reviews 
      ✓ should add a review for Drive(2011) made by user2 and return status 201

  Actors endpoint
    GET /api/actors/tmdb/popular 
      ✓ should return 20 actors and a status 200 (178ms)
    GET /api/actors/tmdb/trending 
      ✓ should return 20 trending actors and a status 200 (140ms)
    GET /api/actors/tmdb/actor/:id 
      ✓ should return the actor Jason Statham and a status 200 (112ms)
    GET /api/actors/tmdb/actor/:id/movie-credits 
      ✓ should return movies Jason Statham has a credit in and a status 200 (120ms)

  Genres endpoint
    GET /api/genres 
      ✓ should return 19 genres and a status 200

  Users endpoint
    GET /api/users 
      ✓ should return the 2 users and a status 200
    POST /api/users 
      For a register action
        when the payload is correct
          ✓ should return a 201 status and the confirmation message (224ms)
        when the password is not valid
          ✓ should return a 401 status and the error message
      For an authenticate action
        when the payload is correct
          ✓ should return a 200 status and a generated token (230ms)
        when the username does not exist
          ✓ should return a 401 status and an error message (52ms)
        when the password is incorrect
          ✓ should return a 401 status and an error message (295ms)
        when no username or password are passed
          ✓ should return a 401 status and an error message
      GET /api/users/:userName/favourites
        ✓ should return user1's favourites and status 200
      GET /api/users/:userName/favourites
        ✓ should return user1's favourites and status 200 (46ms)
      POST /api/users/:userName/favourites
        ✓ should add a movie id to user2's favourites and status 201 (267ms)
      POST /api/users/:userName/movie/:id/favourites
        ✓ should remove a movie id from user1's favourites and status 201 (89ms)


  28 passing (16s)

~~~

​
## Independent Learning [Option A]

I extracted the reviews endpoint into a seperate repository which I then deployed using vercel.<br>
Using Postman I have gathered images of the vercel deployment working without issue.<br> you can find the repository [here](https://github.com/Bobbins228/asp-ca2-vercel)

+ https://asp-ca2-vercel.vercel.app/api/reviews/movie/436270/reviews <br>

**Creating a review**
![An image of Postman sending a post request to vercel to create a review](https://res.cloudinary.com/dtstgkwxx/image/upload/v1671827622/pm_j2agqe.png "Posting a movie review")
**Getting a list of reviews**
![An image of Postman sending a get request to vercel](https://res.cloudinary.com/dtstgkwxx/image/upload/v1671827829/pm2_hrzqhj.png "Getting movie reviews based on an id")
**Updating a review**
![An image of Postman sending a post request to vercel to update a review](https://res.cloudinary.com/dtstgkwxx/image/upload/v1671827989/pm3_n3p8pf.png "Updating a movie review")
​
## Other Independant learning
Using a Raspberry Pi running Ubuntu server I was able to run the api express app.<br>
Using cloudflare Zero Trust tunnels and a personal domain anyone can access the api from anywhere in the world.
Although it took some time I found this to be a great free alternative hosting option but the only issue with this is that because I have an older raspberry pi the performance is a bit slow so things may take a while to load.
This is much like the vercel deployment option but instead of one endpoint being deployed all of my endpoints are deployed.
You can use the api at this address.<br> https://api.markscampbell.com/api/movies/tmdb/discover